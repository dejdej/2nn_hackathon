########### Python 3.6 #############
import requests, sys
import flask

headers = {
    # Request headers 
    'Ocp-Apim-Subscription-Key': '8644add1221840dcbb6ac1a116fd71cf',
}

# params = {
#     # Query parameter
#     'q': 'def something',
#     # Optional request parameters, set to default values
#     'timezoneOffset': '0',
#     'verbose': 'false',
#     'spellCheck': 'false',
#     'staging': 'true',
# }


def open_code_snippet(snippet_name):
    """take the two first lines from each code snippet"""
    with open (snippet_name,'rb') as f:
        #first_line,second_line = f.readlines()[:2]
        first_two_lines = f.readlines()[:2]
    return first_two_lines    


def get_code_snippet(codeline):
    """define parameters as dict"""
    params = {
    # Query parameter
    'q': str(codeline),
    # Optional request parameters, set to default values
    'timezoneOffset': '0',
    'verbose': 'true',
    'spellCheck': 'false',
    'staging': 'false',
        }
    return params

        
 


if __name__ == "__main__":

    code_line = open_code_snippet("snippet3.java")[0]
    print("{0} of type {1}".format(code_line,type(code_line)))

    code = get_code_snippet(code_line)

    try:
        r = requests.get('https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/a648c673-3532-48d2-86b2-628b3817af9b?subscription-key=8644add1221840dcbb6ac1a116fd71cf', headers=headers, params=code)
        print(r.json())
    except Exception as e:
        print("[Errno {0}] {1}".format(e.errno, e.strerror))



