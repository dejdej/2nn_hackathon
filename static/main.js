(function() { 'use strict'; angular.module('SnippyApp', [])

  .controller('SnippyController', ['$scope', '$log', '$http', function($scope, $log, $http) {
      $scope.submitButtonText = 'Submit';
      $scope.loading = false;

      $scope.readFile = function (ele) {
        var reader = new FileReader();
        reader.onload = function () {
          $scope.fileSnippet = reader.result;
          $scope.$apply();
        }
        reader.readAsText(ele.files[0]);
      }

      $scope.textResults = function() {
        getResults($scope.textSnippet);
      };

      $scope.fileResults = function() {
        getResults($scope.fileSnippet);
      };

      function getResults(codeSnippet) {
        var apiUrl = 'https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/a648c673-3532-48d2-86b2-628b3817af9b';
        $scope.loading = true;

        $http({
          method: 'GET',
          url: apiUrl,
          headers: { 'Ocp-Apim-Subscription-Key': '8644add1221840dcbb6ac1a116fd71cf' },
          params: {
            q: codeSnippet,
            verbose: 'true',
            staging: 'false'
          }
        }).
        success(function(results) {
          $log.log(results);
          $scope.results = parse(results);
          $scope.loading = false;
        }).
        error(function(error) {
          $log.log(error);
          $scope.loading = false;
        });

        function parse(results) {
          var intents = results.intents.map(function(r) {
            return { name: r.intent, score: r.score.toFixed(4) };
          });
          var entities = results.entities.map(function(r) {
            return { name: '"' + r.entity + '" ' + r.type, score: r.score.toFixed(4) };
          });
          return { intents: intents, entities: entities };
        };
      };
  }])
}());